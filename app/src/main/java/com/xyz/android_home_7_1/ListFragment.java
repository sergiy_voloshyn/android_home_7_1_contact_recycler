package com.xyz.android_home_7_1;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListFragment extends Fragment {

    @BindView(R.id.list)
    RecyclerView list;
    ContactRecyclerAdapter adapter;
    ElementClickListener elementClickListener;

    ArrayList<Contact> contacts;


    public static ListFragment newInstance() {
        return new ListFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ElementClickListener) {
            elementClickListener = (ElementClickListener) context;
        } else {
            throw new RuntimeException("Must be MainActivity");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, view);

        adapter = new ContactRecyclerAdapter(contacts, getContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        list.setLayoutManager(layoutManager);
        list.setAdapter(adapter);

        adapter.setOnContactClick(new ContactRecyclerAdapter.OnContactClick() {
            @Override
            public void onItemClick(int position) {
                Contact contact = adapter.getItem(position);
                elementClickListener.onItemClick(contact);
            }
        });
        return view;
    }

    public void setList(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }


}
