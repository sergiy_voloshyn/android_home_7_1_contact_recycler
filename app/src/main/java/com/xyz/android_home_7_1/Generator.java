package com.xyz.android_home_7_1;

import com.xyz.android_home_7_1.Contact;
import com.xyz.android_home_7_1.R;

import java.util.ArrayList;

/**
 * Created by user on 21.01.2018.
 */

public final class Generator {

    public Generator() {
    }

    public static ArrayList<Contact> generate() {
        int sizeList = 12;
        ArrayList<Contact> contacts = new ArrayList<Contact>();

        Integer[] images = {
                R.drawable.image_1,
                R.drawable.image_2,
                R.drawable.image_3,
                R.drawable.image_4,
                R.drawable.image_5,
                R.drawable.image_6,
                R.drawable.image_7,
                R.drawable.image_8,
                R.drawable.image_9,
                R.drawable.image_10,
                R.drawable.image_11,
                R.drawable.image_12

        };


        for (int i = 0; i < sizeList; i++) {

            contacts.add(new Contact("Name " + i, "email " + i, "address " + i, "phone " + i, images[i]));

        }
        return contacts;


    }


}