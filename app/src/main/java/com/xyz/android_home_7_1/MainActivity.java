package com.xyz.android_home_7_1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import static android.content.res.Configuration.ORIENTATION_PORTRAIT;

public class MainActivity extends AppCompatActivity implements ElementClickListener {

    boolean isPortrait;

    ListFragment listFragment;
    ItemFragment itemFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        isPortrait = getResources().getConfiguration().orientation == ORIENTATION_PORTRAIT;

        ArrayList<Contact> contacts = Generator.generate();

        listFragment = ListFragment.newInstance();
        listFragment.setList(contacts);
        itemFragment = ItemFragment.newInstance();
        itemFragment.updateContact(contacts.get(0));

        if (isPortrait) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.base_container, listFragment)
                    .commit();
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.left_container, listFragment)
                    .replace(R.id.right_container, itemFragment)
                    .commit();
        }

    }

    @Override
    public void onItemClick(Contact contact) {

        itemFragment.updateContact(contact);

        if (isPortrait) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.base_container, itemFragment)
                    .addToBackStack(null)
                    .commit();
        }

    }
}
