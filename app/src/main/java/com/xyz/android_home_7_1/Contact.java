package com.xyz.android_home_7_1;

/**
 * Created by user on 25.01.2018.
 */

public class Contact {
    private String name;
    private String email;
    private String address;
    private String phone;
    private Integer photo;

    public Contact(String name, String email, String address, String phone, Integer photo) {
        this.name = name;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.photo = photo;
    }

    public Contact() {

        this.name = "name1";
        this.email = "email1";
        this.address = "address1";
        this.phone = "phone1";
        this.photo = R.drawable.image_10;

    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public Integer getPhoto() {
        return photo;
    }
}
